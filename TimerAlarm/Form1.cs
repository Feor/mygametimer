﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimerAlarm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DateTime current = new DateTime(),
            countStartedAt = new DateTime(),
            pausedAt = new DateTime();

        private void tmTick_Tick(object sender, EventArgs e)
        {
            current = DateTime.Now;
            var span = current - countStartedAt;

            lbTimer.Text = span.Hours + ":" + span.Minutes + ":" + span.Seconds + "." + span.Milliseconds;//.ToString("HH:mm:ss.fff"); 

            if (span.Hours >= 1)
                lbTimer.ForeColor = Color.Red;
            else
                lbTimer.ForeColor = Color.Green;
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            countStartedAt = DateTime.Now;

            tmTick.Enabled = true;
        }

        private void btPause_Click(object sender, EventArgs e)
        {

            if (tmTick.Enabled)
            {
                pausedAt = DateTime.Now;
                tmTick.Enabled = false;
                btPause.Text = "unpause";
            }
            else
            {
                countStartedAt = countStartedAt + (pausedAt - countStartedAt);
                tmTick.Enabled = true;
                btPause.Text = "pause";
            }
        }

        private void btReset_Click(object sender, EventArgs e)
        {
            countStartedAt = DateTime.Now;
        }
    }
}
